# Lapres Final Praktikum SISOP 2023

## anggota
| Nama                   | NRP        |
-------------------------|------------|
| Monika Damelia Hutapea | 5027221011 |
| Muhammad Faishal Rizqy | 5027221026 |
| Ditya Wahyu Ramadhan   | 5027221051 |

## client.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4444

struct allowed
{
    char name[10000];
    char password[10000];
};

// DEKLARASI FUNGSI
int cekAllowed(char *username, char *password);

void writelog(char *perintah, char *nama);

int main(int argc, char *argv[])
{
    int allowed = 0;

    int id_user = geteuid();

    char database_used[1000];

    if (geteuid() == 0)
    {
        allowed = 1;
    }

    else
    {
        int id = geteuid();
        allowed = cekAllowed(argv[2], argv[4]);
    }

    if (allowed == 0)
    {
        return 0;
    }

    int clientSocket, ret;

    struct sockaddr_in serverAddr;

    char buffer[32000];

    clientSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (clientSocket < 0)
    {
        printf("Koneksi gagal, silakan coba lagi!!\n");
        exit(1);
    }

    printf("Socket Client telah dibuat.\n");

    memset(&serverAddr, '\0', sizeof(serverAddr));

    serverAddr.sin_family = AF_INET;

    serverAddr.sin_port = htons(PORT);

    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    ret = connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    if (ret < 0)
    {
        printf("Koneksi gagal, silakan coba lagi!!\n");
        exit(1);
    }
    printf("Terhubung ke server.\n");

    while (1)
    {
        printf("Enter command (EXIT for close): ");
        char input[10000];
        char copyinput[10000];
        char perintah[100][10000];
        char *token;
        int i = 0;
        scanf(" %[^\n]s", input);
        strcpy(copyinput, input);
        token = strtok(input, " ");
        while (token != NULL)
        {
            strcpy(perintah[i], token);
            i++;
            token = strtok(NULL, " ");
        }
        if (strcmp(perintah[0], "EXIT") == 0)
        {
            snprintf(buffer, sizeof buffer, "%s", perintah[0]);
            send(clientSocket, buffer, strlen(buffer), 0);
            break;
        }
        int wrongCommand = 0;

        if (strcmp(perintah[0], "CREATE") == 0)
        {
            if (strcmp(perintah[1], "USER") == 0 && strcmp(perintah[3], "IDENTIFIED") == 0 && strcmp(perintah[4], "BY") == 0)
            {
                snprintf(buffer, sizeof buffer, "cUser:%s:%s:%d", perintah[2], perintah[5], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            }

            else if (strcmp(perintah[1], "TABLE") == 0)
            {
                snprintf(buffer, sizeof buffer, "cTable:%s", copyinput);
                send(clientSocket, buffer, strlen(buffer), 0);
            }

            else if (strcmp(perintah[1], "DATABASE") == 0)
            {
                snprintf(buffer, sizeof buffer, "cDatabase:%s:%s:%d", perintah[2], argv[2], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        }

        else if (strcmp(perintah[0], "USE") == 0)
        {
            snprintf(buffer, sizeof buffer, "uDatabase:%s:%s:%d", perintah[1], argv[2], id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
        }

        else if (strcmp(perintah[0], "GRANT") == 0 && strcmp(perintah[1], "PERMISSION") == 0 && strcmp(perintah[3], "INTO") == 0)
        {
            snprintf(buffer, sizeof buffer, "gPermission:%s:%s:%d", perintah[2], perintah[4], id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
        }

        else if (strcmp(perintah[0], "SELECT") == 0)
        {
            snprintf(buffer, sizeof buffer, "select:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        }

        else if (strcmp(perintah[0], "cekCurrentDatabase") == 0)
        {
            snprintf(buffer, sizeof buffer, "%s", perintah[0]);
            send(clientSocket, buffer, strlen(buffer), 0);
        }

        else if (strcmp(perintah[0], "UPDATE") == 0)
        {
            snprintf(buffer, sizeof buffer, "update:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        }

        else if (strcmp(perintah[0], "DROP") == 0)
        {
            if (strcmp(perintah[1], "DATABASE") == 0)
            {
                snprintf(buffer, sizeof buffer, "dDatabase:%s:%s", perintah[2], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            }

            else if (strcmp(perintah[1], "COLUMN") == 0)
            {
                snprintf(buffer, sizeof buffer, "dColumn:%s:%s:%s", perintah[2], perintah[4], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            }

            else if (strcmp(perintah[1], "TABLE") == 0)
            {
                snprintf(buffer, sizeof buffer, "dTable:%s:%s", perintah[2], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        }

        else if (strcmp(perintah[0], "DELETE") == 0)
        {
            snprintf(buffer, sizeof buffer, "delete:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        }

        else if (strcmp(perintah[0], "INSERT") == 0 && strcmp(perintah[1], "INTO") == 0)
        {
            snprintf(buffer, sizeof buffer, "insert:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        }

        else if (strcmp(perintah[0], ":exit") != 0)
        {
            wrongCommand = 1;
            char peringatan[] = "Perintah salah!!";
            send(clientSocket, peringatan, strlen(peringatan), 0);
        }

        if (wrongCommand != 1)
        {
            char namaSender[10000];

            if (id_user == 0)
            {
                strcpy(namaSender, "root");
            }

            else
            {
                strcpy(namaSender, argv[2]);
            }
            writelog(copyinput, namaSender);
        }

        bzero(buffer, sizeof(buffer));

        if (recv(clientSocket, buffer, 1024, 0) < 0)
        {
            printf("Gagal menerima data!!\n");
        }

        else
        {
            printf("Server: \t%s\n", buffer);
        }
    }

    close(clientSocket);

    printf("Terputus dari Server.\n");

    return 0;
}

int cekAllowed(char *username, char *password)
{
    FILE *fp;
    struct allowed user;

    int id, found = 0;
    fp = fopen("../database/databases/user.dat", "rb");

    while (1)
    {
        fread(&user, sizeof(user), 1, fp);
        if (strcmp(user.name, username) == 0)

        {
            if (strcmp(user.password, password) == 0)
            {
                found = 1;
            }
        }

        if (feof(fp))
        {
            break;
        }
    }

    fclose(fp);

    if (found == 0)
    {
        printf("Anda tidak diizinkan\n");
        return 0;
    }

    else
    {
        return 1;
    }
}

void writelog(char *perintah, char *nama)
{
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    char infoWriteLog[1000];

    FILE *file;
    char lokasi[10000];

    snprintf(lokasi, sizeof lokasi, "../database/databases/log%s.log", nama);
    file = fopen(lokasi, "ab");

    sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, perintah);

    fputs(infoWriteLog, file);
    fclose(file);
    return;
}
```

- Struct allowed:
Ini adalah definisi struktur allowed yang memiliki dua anggota: name untuk menyimpan nama pengguna (username) dan password untuk menyimpan kata sandi (password).
```c
struct allowed
{
    char name[10000];
    char password[10000];
};
```

- cekAllowed Function:
Fungsi ini membaca file biner "../database/databases/user.dat" yang berisi informasi nama pengguna dan kata sandi yang diizinkan. Fungsi ini memeriksa apakah username dan password yang diberikan sesuai dengan data yang ada di file. Jika sesuai, fungsi mengembalikan 1; jika tidak, mencetak pesan "Anda tidak diizinkan" dan mengembalikan 0.
*1*	char *username: Nama pengguna yang akan diperiksa izinnya.
*2*	char *password: Kata sandi yang akan digunakan untuk verifikasi izin.
*3*	1 jika pengguna diizinkan.
*4*	0 jika pengguna tidak diizinkan
```c
int cekAllowed(char *username, char *password)
{
    FILE *fp;
    struct allowed user;

    int id, found = 0;
    fp = fopen("../database/databases/user.dat", "rb");

    while (1)
    {
        fread(&user, sizeof(user), 1, fp);
        if (strcmp(user.name, username) == 0)

        {
            if (strcmp(user.password, password) == 0)
            {
                found = 1;
            }
        }

        if (feof(fp))
        {
            break;
        }
    }

    fclose(fp);

    if (found == 0)
    {
        printf("Anda tidak diizinkan\n");
        return 0;
    }

    else
    {
        return 1;
    }
}
```

- writelog Function:
Fungsi ini menulis log ke file dengan format timestamp, nama pengguna, dan perintah yang dilakukan. Log ditulis dalam format tertentu dan disimpan dalam file yang berbeda untuk setiap pengguna.
*1*	char *perintah: Perintah yang akan dicatat dalam log.
*2*	char *nama: Nama pengguna yang melakukan perintah.
```c
void writelog(char *perintah, char *nama)
{
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    char infoWriteLog[1000];

    FILE *file;
    char lokasi[10000];

    snprintf(lokasi, sizeof lokasi, "../database/databases/log%s.log", nama);
    file = fopen(lokasi, "ab");

    sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, perintah);

    fputs(infoWriteLog, file);
    fclose(file);
    return;
}
```

- Fungsi main:
Membuat koneksi ke server menggunakan socket.
Melakukan loop utama untuk menerima perintah dari pengguna, mengirimkannya ke server, dan menanggapi balasan dari server.
Menuliskan log menggunakan fungsi writelog untuk setiap perintah yang valid.
Program berakhir saat pengguna memasukkan perintah "EXIT".

- Parameter:
int argc: Jumlah argumen dari baris perintah.
char *argv[]: Array dari string yang berisi argumen dari baris perintah.
Return Value: 0 pada keluar program.


## database.c

- Bagian import library dan define.
```c
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>

#define PORT 4444
#define MAXDATALEN 200
```

- Bagian struct, terdapat tiga struct yakni table, allowed_database, dan allowed. 
```c
struct table
{
	int jumlahkolom;
	char type[100][10000];
	char data[100][10000];
};

struct allowed_database
{
	char database[10000];
	char name[10000];
};

struct allowed
{
	char name[10000];
	char password[10000];
};
```

- Bagian deklarasi semua fungsi.
```c
void daemonize();

void createUser(char *nama, char *password);

int cekUserExist(char *username);

void insertPermission(char *nama, char *database);

int cekAllowedDatabase(char *nama, char *database);

int findColumn(char *table, char *kolom);

int deleteColumn(char *table, int index);

int updateColumn(char *table, int index, char *ganti);

int updateColumnWhere(char *table, int index, char *ganti, int indexGanti, char *where);

int deleteTableWhere(char *table, int index, char *kolom, char *where);

void writelog(char *perintah, char *nama);

int deleteTable(char *table, char *namaTable);
```

- Bagian fungsi daemonize untuk membuat program berjalan secara daemon.
```c
void daemonize()
{
    pid_t pid, sid;

    pid = fork();

    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}
```

- Bagian fungsi utama.
```c
int main()
{
	 //SETUP DAEMON
    char workDir[MAXDATALEN];
    getcwd(workDir, sizeof(workDir));

    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
   
    if (pid > 0) {
        printf("PID : %d || %sn\n", pid, workDir);
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir(workDir)) < 0) {
        
        exit(EXIT_FAILURE);
    }	

    // bagian pembuatan socket
	int sockfd, ret;
	struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		printf("Koneksi Error.\n");
		exit(1);
	}
	printf("Socket Server Berhasil Dibuat.\n");

    // bagian binding socker
	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
	if (ret < 0)
	{
		printf("Gagal binding.\n");
		exit(1);
	}
	printf("Bind to port %d\n", 4444);

    // listening connection
	if (listen(sockfd, 10) == 0)
	{
		printf("Sedang berjalan...\n");
	}
	else
	{
		printf("Gagal binding.\n");
	}
```

- Didalam fungsi utama, terdapat loop utama yang dimana isinya adalah komunikasi antar client dan database.
```c
	while (1)
	{
		newSocket = accept(sockfd, (struct sockaddr *)&newAddr, &addr_size);
		if (newSocket < 0)
		{
			exit(1);
		}
		printf("Koneksi diterima dari %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port)); // koneksi diterima dan memulai komunikasi dengan client
```
- Fork child process untuk handle koneksi client.
```c
		if ((childpid = fork()) == 0)
		{
			close(sockfd);
			while (1)
			{
```
- Bagian untuk parsing perintah yang diterima dari client.
```c
				recv(newSocket, buffer, 1024, 0);
				char *token;
				char buffercopy[32000];
				strcpy(buffercopy, buffer);
				char perintah[100][10000];
				token = strtok(buffercopy, ":");
				int i = 0;
				char database_used[1000];
				while (token != NULL)
				{
					strcpy(perintah[i], token);
					i++;
					token = strtok(NULL, ":");
				}
```

- Bagian menerima perintah untuk membuat user baru, apabila diterima (perintah[3] = 0, (artinya client merupakan root)), maka dibuat user baru, apabila tidak, maka tidak diizinkan untuk membuat user baru.
```c
				if (strcmp(perintah[0], "cUser") == 0)
				{
					if (strcmp(perintah[3], "0") == 0) // client root
					{
						createUser(perintah[1], perintah[2]);
					}
					else // client bukan root
					{
						char peringatan[] = "Tidak diizinkan";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
					}
				}
```
- Bagian menerima perintah untuk membuat database baru.
```c
				else if (strcmp(perintah[0], "cDatabase") == 0)
				{
					char lokasi[20000];
					snprintf(lokasi, sizeof lokasi, "databases/%s", perintah[1]);
					printf("lokasi = %s, nama = %s , database = %s\n", lokasi, perintah[2], perintah[1]);
					mkdir(lokasi, 0777);
					insertPermission(perintah[2], perintah[1]);
				}
```

- Bagian menerima perintah untuk memberikan permission pada user.
```c
				else if (strcmp(perintah[0], "gPermission") == 0)
				{
					if (strcmp(perintah[3], "0") == 0) // client adalah root
					{
						int exist = cekUserExist(perintah[2]);
						if (exist == 1)
						{
							insertPermission(perintah[2], perintah[1]);
						}
						else // apabila user yang akan diberi akses tidak ditemukan
						{
							char peringatan[] = "User tidak ditemukan";
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
					else // apabila client bukan root
					{
						char peringatan[] = "Anda tidak diizinkan";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
					}
				}
```

- Bagian menerima untuk perintah mengecek database.
```c
				else if (strcmp(perintah[0], "cekCurrentDatabase") == 0)
				{
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "Anda belum memilih database");
					}
					send(newSocket, database_used, strlen(database_used), 0);
					bzero(buffer, sizeof(buffer));
				}
```

- Bagian menerima perintah untuk menggunakan database.
```c
				else if (strcmp(perintah[0], "uDatabase") == 0)
				{
					if (strcmp(perintah[3], "0") != 0)
					{
						int allowed = cekAllowedDatabase(perintah[2], perintah[1]); // mengecek apakah user diperbolehkan
						if (allowed != 1) // akses tidak diizinkan
						{
							char peringatan[] = "Akses_database : Anda tidak diizinkan";
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
						}
						else // akses diizinkan
						{
							strncpy(database_used, perintah[1], sizeof(perintah[1]));
							char peringatan[] = "Akses_database : Diizinkan";
							printf("database_used = %s\n", database_used);
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
				}
```

- Bagian menerima perintah untuk membuat tabel baru.
```c
				else if (strcmp(perintah[0], "cTable") == 0)
				{
					printf("%s\n", perintah[1]);
					char *tokens;
					if (database_used[0] == '\0') // user belum memilih database
					{
						strcpy(database_used, "Anda belum memilih database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
					}
					else // user sudah memilih database
					{
                        // proses untuk parsing perintah
						char daftarQuery[100][10000];
						char copyPerintah[20000];
						snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
						tokens = strtok(copyPerintah, "(), ");
						int jumlah = 0;
						while (tokens != NULL)
						{
							strcpy(daftarQuery[jumlah], tokens);
							printf("%s\n", daftarQuery[jumlah]);
							jumlah++;
							tokens = strtok(NULL, "(), ");
						}

                        // 
						char buatTable[20000];
						snprintf(buatTable, sizeof buatTable, "../database/databases/%s/%s", database_used, daftarQuery[2]);
						int iterasi = 0;
						int iterasiData = 3;
						struct table kolom;

                        // 
						while (jumlah > 3)
						{
							strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
							printf("%s\n", kolom.data[iterasi]);
							strcpy(kolom.type[iterasi], daftarQuery[iterasiData + 1]);
							iterasiData = iterasiData + 2;
							jumlah = jumlah - 2;
							iterasi++;
						}

                        // memasukan data ke database
						kolom.jumlahkolom = iterasi;
						printf("iterasi = %d\n", iterasi);
						FILE *fp;
						printf("%s\n", buatTable);
						fp = fopen(buatTable, "ab");
						fwrite(&kolom, sizeof(kolom), 1, fp);
						fclose(fp);
					}
				}
```

- Bagian menerima perintah untuk menghapus table.
```c
				else if (strcmp(perintah[0], "dTable") == 0)
				{
					if (database_used[0] == '\0') // apabila belum menggunakan USE DATABASE
					{
						strcpy(database_used, "Anda belum memilih database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    // bagian menghapus database
					char hapus[20000]; 
					snprintf(hapus, sizeof hapus, "databases/%s/%s", database_used, perintah[1]);
					remove(hapus);
					char peringatan[] = "Table telah dihapus";
					send(newSocket, peringatan, strlen(peringatan), 0);
					bzero(buffer, sizeof(buffer));
				}
```

- Bagian menerima perintah untuk menghapus database.
```c
                else if (strcmp(perintah[0], "dDatabase") == 0)
				{
                    // bagian mengecek akses
					int allowed = cekAllowedDatabase(perintah[2], perintah[1]);
					if (allowed != 1)
					{
						char peringatan[] = "Akses_database : Anda tidak diizinkan";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    // bagian menghapus database
					else 
					{
						char hapus[20000];
						snprintf(hapus, sizeof hapus, "rm -r databases/%s", perintah[1]);
						system(hapus);
						char peringatan[] = "Database telah dihapus";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
					}
				}
```

- Bagian menerima perintah untuk insert data.
```c
				else if (strcmp(perintah[0], "insert") == 0)
				{
                    // apabila user belum memilih database
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "Anda belum memilih database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    // parsing data
					char daftarQuery[100][10000];
					char copyPerintah[20000];
					snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
					char *tokens;
					tokens = strtok(copyPerintah, "\'(), ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(daftarQuery[jumlah], tokens);
						jumlah++;
						tokens = strtok(NULL, "\'(), ");
					}
					char buatTable[20000];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[2]);
					FILE *fp;
					int banyakKolom;
					fp = fopen(buatTable, "r");
					// table tidak ada
                    if (fp == NULL)
					{
						char peringatan[] = "Table tidak ditemukan";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    // membuat struct
					else
					{
						struct table user;
						fread(&user, sizeof(user), 1, fp);
						banyakKolom = user.jumlahkolom;
						fclose(fp);
					}
                    // memasukan data 
					int iterasi = 0;
					int iterasiData = 3;
					struct table kolom;
					while (jumlah > 3)
					{
						strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
						printf("%s\n", kolom.data[iterasi]);
						strcpy(kolom.type[iterasi], "string");
						iterasiData++;
						jumlah = jumlah - 1;
						iterasi++;
					}
					kolom.jumlahkolom = iterasi;
					if (banyakKolom != kolom.jumlahkolom)
					{
						char peringatan[] = "Input Anda tidak cocok dengan kolom";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					printf("iterasi = %d\n", iterasi);
					FILE *fp1;
					printf("%s\n", buatTable);
					fp1 = fopen(buatTable, "ab");
					fwrite(&kolom, sizeof(kolom), 1, fp1);
					fclose(fp1);
					char peringatan[] = "Data telah dimasukkan";
					send(newSocket, peringatan, strlen(peringatan), 0);
					bzero(buffer, sizeof(buffer));
				}
```

- Bagian menerima perintah untuk menghapus kolom.
```c
				else if (strcmp(perintah[0], "dColumn") == 0)
				{
                    // apabila user belum memilih database
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "Anda belum memilih database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    // mencari tabel
					char buatTable[20000];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, perintah[2]);
					int index = findColumn(buatTable, perintah[1]);
                    // apabila kolom tidak ditemukan
					if (index == -1)
					{
						char peringatan[] = "Kolom tidak ditemukan";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    // bagian menghapus kolom
					deleteColumn(buatTable, index);
					char peringatan[] = "Kolom telah dihapus";
					send(newSocket, peringatan, strlen(peringatan), 0);
					bzero(buffer, sizeof(buffer));
				}
```

- Bagian menerima perintah untuk menghapus data.
```c
				else if (strcmp(perintah[0], "delete") == 0)
				{
                    // apabila belum memilih database
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "Anda belum memilih database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char daftarQuery[100][10000];
					char copyPerintah[20000];
					snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
					char *tokens;
					tokens = strtok(copyPerintah, "\'(),= ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("jumlah = %d\n", jumlah);
					char buatTable[20000];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[2]);
					if (jumlah == 3)
					{
						deleteTable(buatTable, daftarQuery[2]);
					}
					else if (jumlah == 6)
					{
						int index = findColumn(buatTable, daftarQuery[4]);
						if (index == -1)
						{
							char peringatan[] = "Kolom tidak ditemukan";
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
							continue;
						}
						printf("index  = %d\n", index);
						deleteTableWhere(buatTable, index, daftarQuery[4], daftarQuery[5]);
					}
					else // input salah
					{
						char peringatan[] = "Input Salah";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char peringatan[] = "Data telah dihapus";
					send(newSocket, peringatan, strlen(peringatan), 0);
					bzero(buffer, sizeof(buffer));
				}
```

- Bagian menerima perintah untuk select.
```c
                else if (strcmp(perintah[0], "select") == 0)
				{
                    // apabila user belum memilih database
					if (database_used[0] == '\0')
					{
						strcpy(database_used, "Anda belum memilih database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    //  bagian parsing perintah
					char daftarQuery[100][10000];
					char copyPerintah[20000];
					snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
					char *tokens;
					tokens = strtok(copyPerintah, "\'(),= ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("ABC\n");
                    // apabila perintah berupa 4 string
					if (jumlah == 4)
					{
						char buatTable[20000];
						snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[3]);
						printf("buat table = %s", buatTable);
						char perintahKolom[1000];
						printf("masuk 4\n");
                        // bagian untuk membuka semua tabel
						if (strcmp(daftarQuery[1], "*") == 0)
						{
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(buatTable, "rb");
							char buffers[40000];
							char sendDatabase[40000];
							bzero(buffer, sizeof(buffer));
							bzero(sendDatabase, sizeof(sendDatabase));
                            // membaca data tabel dan memasukan data ke buffer untuk di kirim
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(buffers, sizeof buffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.jumlahkolom; i++)
								{
									char padding[20000];
									snprintf(padding, sizeof padding, "%s\t", user.data[i]);
									strcat(buffers, padding);
								}
								strcat(sendDatabase, buffers);
							}
                            // mengirim data ke client dan menutup file 
							send(newSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(buffer, sizeof(buffer));
							fclose(fp);
						}
						else
						{
                            // mencari tabel
                        	int index = findColumn(buatTable, daftarQuery[1]);
							printf("%d\n", index);
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(buatTable, "rb");
							char buffers[40000];
							char sendDatabase[40000];
							bzero(buffer, sizeof(buffer));
							bzero(sendDatabase, sizeof(sendDatabase));
                            // membaca data tabel dan memasukan data ke buffer untuk di kirim
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(buffers, sizeof buffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.jumlahkolom; i++)
								{
									if (i == index)
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(buffers, padding);
									}
								}
								strcat(sendDatabase, buffers);
							}
                            // mengirim data ke client dan menutup file 
							printf("ini send fix\n%s\n", sendDatabase);
							fclose(fp);
							send(newSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(buffer, sizeof(buffer));
						}
					}
                    // apabila perintah berupa 7 string
					else if (jumlah == 7 && strcmp(daftarQuery[4], "WHERE") == 0)
					{
						char buatTable[20000];
						snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[3]);
						printf("buat table = %s", buatTable);
						char perintahKolom[1000];
						printf("masuk 4\n");
						if (strcmp(daftarQuery[1], "*") == 0)
						{
                            // membuka file
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(buatTable, "rb");
							char buffers[40000];
							char sendDatabase[40000];
							int index = findColumn(buatTable, daftarQuery[5]);
							printf("%d\n", index);
							bzero(buffer, sizeof(buffer));
							bzero(sendDatabase, sizeof(sendDatabase));
                            // tabel
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(buffers, sizeof buffers, "\n");
								if (feof(fp))
								{
									break;
								}
                                // kolom
								for (int i = 0; i < user.jumlahkolom; i++)
								{
									if (strcmp(user.data[index], daftarQuery[6]) == 0)
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(buffers, padding);
									}
								}
								strcat(sendDatabase, buffers);
							}
							send(newSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(buffer, sizeof(buffer));
							fclose(fp);
						}
						else
						{
                            // membuka file
							int index = findColumn(buatTable, daftarQuery[1]);
							printf("%d\n", index);
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							int indexGanti = findColumn(buatTable, daftarQuery[5]);
							fp = fopen(buatTable, "rb");
							char buffers[40000];
							char sendDatabase[40000];
							bzero(buffer, sizeof(buffer));
							bzero(sendDatabase, sizeof(sendDatabase));
                            // tabel
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(buffers, sizeof buffers, "\n");
								if (feof(fp))
								{
									break;
								}
                                // kolom
								for (int i = 0; i < user.jumlahkolom; i++)
								{
									if (i == index && (strcmp(user.data[indexGanti], daftarQuery[6]) == 0 || strcmp(user.data[i], daftarQuery[5]) == 0))
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(buffers, padding);
									}
								}
								strcat(sendDatabase, buffers);
							}
							printf("ini send fix\n%s\n", sendDatabase);
							fclose(fp);
							send(newSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(buffer, sizeof(buffer));
						}
					}
					else
					{
						printf("ini query 3 %s", daftarQuery[jumlah - 3]);
						if (strcmp(daftarQuery[jumlah - 3], "WHERE") != 0)
						{
							char buatTable[20000];
							snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[jumlah - 1]);
							printf("buat table = %s", buatTable);
							printf("tanpa where");
							int index[100];
							int iterasi = 0;
							for (int i = 1; i < jumlah - 2; i++)
							{
								index[iterasi] = findColumn(buatTable, daftarQuery[i]);
								printf("%d\n", index[iterasi]);
								iterasi++;
							}
						}
						else if (strcmp(daftarQuery[jumlah - 3], "WHERE") == 0)
						{
							printf("dengan where");
						}
					}
				}
```

- Bagian menerima perintah untuk update.
```c
				else if (strcmp(perintah[0], "update") == 0)
				{
					if (database_used[0] == '\0') // user belum memilih database
					{
						strcpy(database_used, "Anda belum memilih database");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    // parsing perintah
					char daftarQuery[100][10000];
					char copyPerintah[20000];
					snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
					char *tokens;
					tokens = strtok(copyPerintah, "\'(),= ");
					int jumlah = 0;
                    // mencari query
					while (tokens != NULL)
					{
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}

					printf("jumlah = %d\n", jumlah);
					char buatTable[20000];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[1]);
                    // jumlah token 5
					if (jumlah == 5)
					{
						printf("buat table = %s, kolumn = %s", buatTable, daftarQuery[3]);
						int index = findColumn(buatTable, daftarQuery[3]);
						if (index == -1)
						{
							char peringatan[] = "Kolom tidak ditemukan";
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
							continue;
						}
						printf("index = %d\n", index);
                        // fungsi update kolom
						updateColumn(buatTable, index, daftarQuery[4]);
					}
                    // jumlah token 8
					else if (jumlah == 8)
					{
						printf("buat table = %s, kolumn = %s", buatTable, daftarQuery[3]);
						int index = findColumn(buatTable, daftarQuery[3]);
						if (index == -1)
						{
							char peringatan[] = "Kolom tidak ditemukan";
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
							continue;
						}
                        // update kolom
						printf("%s\n", daftarQuery[7]);
						int indexGanti = findColumn(buatTable, daftarQuery[6]);
						updateColumnWhere(buatTable, index, daftarQuery[4], indexGanti, daftarQuery[7]);
					}
					else // data dihapus
					{
						char peringatan[] = "Data telah dihapus";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    // data telah diupdate
					char peringatan[] = "Data telah diupdate";
					send(newSocket, peringatan, strlen(peringatan), 0);
					bzero(buffer, sizeof(buffer));
				}
```

- Bagian menerima perintah untuk log.
```c
				else if (strcmp(perintah[0], "log") == 0)
				{
                    // menulis log
					writelog(perintah[1], perintah[2]);
					char peringatan[] = "\n";
					send(newSocket, peringatan, strlen(peringatan), 0);
					bzero(buffer, sizeof(buffer));
				}
```

- Bagian menerima perintah untuk exit.
```c
				if (strcmp(buffer, ":exit") == 0) // break loop utama
				{
					printf("Terputus dari %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				}
				else
				{
                    // tampilkan pesan client
					printf("Client: %s\n", buffer);
					send(newSocket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}
	}
	close(newSocket);
	return 0;
}
```

- Bagian fungsi findColumn.
```c
int findColumn(char *table, char *kolom)
{
    // membuka file
	FILE *fp;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fread(&user, sizeof(user), 1, fp);
	int index = -1;
    // memeriksa setiap kolom
	for (int i = 0; i < user.jumlahkolom; i++)
	{
		if (strcmp(user.data[i], kolom) == 0)
		{
			index = i;
		}
	}
	if (feof(fp))
	{
		return -1; // kolom tidak ditemukan
	}
	fclose(fp); // tutup file
	return index; // tampilkan index yang ditemukan
}
```

- Bagian fungsi cekUserExist.
```c
int cekUserExist(char *username)
{
    // membaca file
	FILE *fp;
	struct allowed user;
	int id, found = 0;
	fp = fopen("../database/databases/user.dat", "rb");
    // cek setiap user pada user,dat
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (strcmp(user.name, username) == 0)
		{
			return 1; // ditemukan
		}
		if (feof(fp))
		{
			break; // keluar apabila file sudah habis
		}
	}
    // tuutp file
	fclose(fp);
	return 0;
}
```

- Bagian fungsi createUser.
```c
void createUser(char *nama, char *password)
{
    // menyimpan data pengguna
	struct allowed user;
    // menyalin nama pengguna
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[] = {"databases/user.dat"};
	FILE *fp;
	fp = fopen(fname, "ab");
    // menulis data
	fwrite(&user, sizeof(user), 1, fp);
	fclose(fp);
}
```

- Bagian fungsi cekAllowedDatabase.
```c
int cekAllowedDatabase(char *nama, char *database)
{
    // membaca file 
	FILE *fp;
	struct allowed_database user;
	int id, found = 0;
	printf("nama = %s  database = %s", nama, database);
	fp = fopen("../database/databases/permission.dat", "rb");
    // membaca izin user pada file
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (strcmp(user.name, nama) == 0)
		{
			if (strcmp(user.database, database) == 0)
			{
				return 1; // izin ditemukan
			}
		}
		if (feof(fp))
		{
			break;
		}
	}
	fclose(fp); // menutup file
	return 0; // izin tidak ditemukan
}
```

- Bagian fungsi insertPermission.
```c
void insertPermission(char *nama, char *database)
{
    // menyimpan data allowed user
	struct allowed_database user;
    // menyalin data user dan membuka file
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[] = {"databases/permission.dat"};
	FILE *fp;
	fp = fopen(fname, "ab");
    // menuliskan user
	fwrite(&user, sizeof(user), 1, fp);
	fclose(fp);
}
```

- Bagian fungsi deleteTable.
```c
int deleteTable(char *table, char *namaTable)
{
    // membaca file
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	fread(&user, sizeof(user), 1, fp);
	int index = -1;
	struct table userCopy;
    // mengecek kolom
	for (int i = 0; i < user.jumlahkolom; i++)
	{
		strcpy(userCopy.data[i], user.data[i]);
		strcpy(userCopy.type[i], user.type[i]);
	}
	userCopy.jumlahkolom = user.jumlahkolom;
	fwrite(&userCopy, sizeof(userCopy), 1, fp1);
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 1;
}
```

- Bagian fungsi deleteTableWhere.
```c
int deleteTableWhere(char *table, int index, char *kolom, char *where)
{
    // membuka file
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
    // membaca tabel
	while (1)
	{
		found = 0;
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.jumlahkolom; i++)
		{
			if (i == index && datake != 0 && strcmp(user.data[i], where) == 0)
			{
				found = 1;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom;
        // menulis ulang
		if (found != 1)
		{
			fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		}
		datake++;
	}
    // menutup file
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
```

- Bagian fungsi updateColumn.
```c
int updateColumn(char *table, int index, char *ganti)
{
    // membuka file
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
    // membaca tabel
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
        // membaca kolom
		for (int i = 0; i < user.jumlahkolom; i++)
		{
			if (i == index && datake != 0)
			{
				strcpy(userCopy.data[iterasi], ganti);
			}
			else
			{
				strcpy(userCopy.data[iterasi], user.data[i]);
			}
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
        // menulis ulang tabel
		userCopy.jumlahkolom = user.jumlahkolom;
		fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		datake++;
	}
    // menutup file
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
```

- Bagian fungsi deleteColumn.
```c
int deleteColumn(char *table, int index)
{
    // membuka file
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "wb");
    // membaca tabel
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
        // membaca tiap kolom
		for (int i = 0; i < user.jumlahkolom; i++)
		{
			if (i == index) // skip kolom yang sama dengan user
			{
				continue;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
        // tulis ulang tabel
		userCopy.jumlahkolom = user.jumlahkolom - 1;
		fwrite(&userCopy, sizeof(userCopy), 1, fp1);
	}
    // menutup file
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
```

- Bagian fungsi writelog.
```c
void writelog(char *perintah, char *nama)
{
    // variabel wakti
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);

	char infoWriteLog[1000];
    //membuka file
	FILE *file;
	file = fopen("logUser.log", "ab");

    // menuliskan log
	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, perintah);
    fputs(infoWriteLog, file);
	fclose(file);
	return;
}
```

- Bagian fungsi updateColumnWhere.
```c
int updateColumnWhere(char *table, int index, char *ganti, int indexGanti, char *where)
{
    // membuka file
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
    // membaca tiap tabel
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
        // membaca tiap kolom
		for (int i = 0; i < user.jumlahkolom; i++)
		{
            // mengganti nilai kolom
			if (i == index && datake != 0 && strcmp(user.data[indexGanti], where) == 0)
			{
				strcpy(userCopy.data[iterasi], ganti);
			}
			else
			{
				strcpy(userCopy.data[iterasi], user.data[i]);
			}
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom;
		fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		datake++;
	}
    // menutup file
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
```

## client_dump.c 
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4444

struct allowed {
    char name[10000];
    char password[10000];
};

int cekAllowed(char *username, char *password) {
    FILE *fp;
    struct allowed user;
    int found = 0;
    fp = fopen("../database/databases/user.dat", "rb");
    if (fp == NULL) {
        printf("Failed to open data.user file.\n");
        return 0;
    }
    while (fread(&user, sizeof(user), 1, fp) == 1) {
        if (strcmp(user.name, username) == 0 && strcmp(user.password, password) == 0) {
            found = 1;
            break;
        }
    }
    fclose(fp);
    if (found == 0) {
        printf("You're Not Allowed\n");
        return 0;
    } else {
        return 1;
    }
}

void removeTimestampAndUsername(char *str) {
    char *pos = strchr(str, ':');
    if (pos != NULL) {
        char *end = strchr(pos + 1, ':');
        if (end != NULL) {
            memmove(str, end + 1, strlen(end + 1) + 1);
            // Menghapus karakter ':' sebelum perintah/command
            for (int i = 0; i < strlen(str); i++) {
                if (str[i] == ':') {
                    memmove(&str[i], &str[i + 1], strlen(str) - i);
                    break;
                }
            }
        }
    }
}

void removeLeadingSpaces(char *str) {
    int i = 0, j = 0;
    while (str[i] != '\0') {
        if (str[i] != ' ') {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}

int main(int argc, char *argv[]) {
    int allowed = 0;
    if (geteuid() == 0) {
        allowed = 1;
    } else {
        allowed = cekAllowed(argv[2], argv[4]);
    }
    if (allowed == 0) {
        return 0;
    }

    FILE *fp;
    char lokasi[10000];
    snprintf(lokasi, sizeof(lokasi), "../database/databases/log%s.log", argv[2]);
    fp = fopen(lokasi, "r");
    if (fp == NULL) {
        printf("Failed to open log file.\n");
        return 0;
    }
    char buffer[20000];
    while (fgets(buffer, sizeof(buffer), fp)) {
        if (strstr(buffer, "CREATE TABLE") || strstr(buffer, "INSERT INTO") || strstr(buffer, "SELECT")) {
            removeTimestampAndUsername(buffer);
            removeLeadingSpaces(buffer);
            printf("%s\n", buffer);
        }
    }
    fclose(fp);
    return 0;
}
```

- Ini adalah definisi struktur allowed yang memiliki dua anggota: name untuk menyimpan nama pengguna (username) dan password untuk menyimpan kata sandi (password).
cekAllowed Function:
```c
struct allowed {
    char name[10000];
    char password[10000];
};
```
- Fungsi ini membaca file biner "../database/databases/user.dat" yang berisi informasi nama pengguna dan kata sandi yang diizinkan. Fungsi ini memeriksa apakah username dan password yang diberikan sesuai dengan data yang ada di file. Jika sesuai, fungsi mengembalikan 1; jika tidak, mencetak pesan "You're Not Allowed" dan mengembalikan 0.
*1*	fread digunakan untuk membaca data dari file biner.
*2*	&user adalah alamat memori dari variabel user. Data dari file akan dibaca dan disimpan di dalam variabel user.
*3*	sizeof(user) menunjukkan ukuran data yang akan dibaca, yaitu ukuran dari struktur data user.
*4*	fp adalah file pointer yang menunjuk ke file yang dibuka.
```if (strcmp(user.name, username) == 0 && strcmp(user.password, password) == 0) {```
*1*	Menggunakan strcmp untuk membandingkan string user.name dengan username dan user.password dengan password.
*1*	Jika kedua perbandingan menghasilkan nilai 0, itu berarti kedua string tersebut identik, sehingga username dan password yang dimasukkan pengguna cocok dengan data yang ada dalam file.
*3*	Jika cocok, found diatur menjadi 1 dan loop dihentikan dengan break.
```c
int cekAllowed(char *username, char *password) {
    FILE *fp;
    struct allowed user;
    int found = 0;
    fp = fopen("../database/databases/user.dat", "rb");
    if (fp == NULL) {
        printf("Failed to open data.user file.\n");
        return 0;
    }
    while (fread(&user, sizeof(user), 1, fp) == 1) {
        if (strcmp(user.name, username) == 0 && strcmp(user.password, password) == 0) {
            found = 1;
            break;
        }
    }
    fclose(fp);
    if (found == 0) {
        printf("You're Not Allowed\n");
        return 0;
    } else {
        return 1;
    }
}
```

- removeTimestampAndUsername Function:
Fungsi ini menghapus timestamp dan nama pengguna dari string yang diberikan. Ini mencari dua titik dua (:) dalam string dan menghapus segmen dari awal hingga setelah kedua titik dua itu.
```c
void removeTimestampAndUsername(char *str) {
    char *pos = strchr(str, ':');
    if (pos != NULL) {
        char *end = strchr(pos + 1, ':');
        if (end != NULL) {
            memmove(str, end + 1, strlen(end + 1) + 1);
            // Menghapus karakter ':' sebelum perintah/command
            for (int i = 0; i < strlen(str); i++) {
                if (str[i] == ':') {
                    memmove(&str[i], &str[i + 1], strlen(str) - i);
                    break;
                }
            }
        }
    }
}
```


- removeLeadingSpaces Function:
Fungsi ini menghapus spasi awal dari string yang diberikan. Ini memindahkan karakter-karakter non-spasi ke awal string dan mengakhiri string dengan null terminator.
```c
void removeLeadingSpaces(char *str) {
    int i = 0, j = 0;
    while (str[i] != '\0') {
        if (str[i] != ' ') {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}
```

## output

### 1. Folder tree

- Folder ~/

![alt text](img/1._folder_all.png)

- Folder ~/client

![alt text](img/1._folder_client.png)

- Folder ~/database

![alt text](img/1._folder_database.png)

- Folder ~/database/databases

![alt text](img/1._folder_databases.png)

- Folder ~/dump

![alt text](img/1._folder_dump.png)


### 2. Running database

- database

![alt text](img/1._run_db.png)

- client

![alt text](img/2._run_client.png)

### 3. Using program

#### a. 

- running

![alt text](img/3._coba_A.png)

![alt text](img/4._coba_B.png)

- hasil

![alt text](img/4._hasil_B.png)

#### b. 

- running

![alt text](img/5._coba_C.png)

![alt text](img/6._coba_D.png)


### 4. Log

![alt text](img/7._E_(log).png)


### 5. Dump

![alt text](img/8._run_dump.png)

![alt text](img/8._hasil_dump.png)

![alt text](img/8._isi_log_dump.png)


### 5. Client backup

![alt text](img/9._run_client_backup.png)

![alt text](img/9._hasil.png)
